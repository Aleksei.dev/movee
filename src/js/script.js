'use strict';

document.addEventListener('DOMContentLoaded', () => {
  //burger
  const icon = document.querySelector('.menu__icon'),
    menuBody = document.querySelector('.menu__body'),
    body     = document.querySelector('body');

  icon.addEventListener('click', () => {
    icon.classList.toggle('active');
    menuBody.classList.toggle('active');
    body.classList.toggle('lock');
  });

  //tabs
  const tabsWrap = document.querySelector('.cars__tabs'),
    tabs         = document.querySelectorAll('.cars__tab'),
    blocks       = document.querySelectorAll('.cars__block');
  const hideBlocks = () => {
    blocks.forEach(block => {
      block.classList.remove('active');
    });
    tabs.forEach(tab => {
      tab.classList.remove('active');
    });
  };

  const showBlock = (i = 0) => {
    blocks[i].classList.add('active');
    tabs[i].classList.add('active');
  };
  hideBlocks();
  showBlock();

  tabsWrap.addEventListener('click', e => {
    const target = e.target;
    if (target && target.classList.contains('cars__tab')) {
      tabs.forEach((tab, index) => {
        if (target === tab) {
          hideBlocks();
          showBlock(index);
        }
      });
    }
  });

  //get data
  // const processBlocks = document.querySelector('.process__blocks');

  // const render = data => {
  //   data.map(item => {
  //     const element = document.createElement('div');
  //     element.classList.add('process__block', 'process-block');
  //     element.innerHTML = `
  //       <img src="${item.img}" alt="один" class="process-block__img">
  //       <div class="process-block__wrap">
  //           <div class="process-block__title">
  //             ${item.title}
  //           </div>
  //           <div class="process-block__desc">
  //             ${item.desc}
  //           </div>
  //       </div>
  //     `;
  //     processBlocks.append(element);
  //   });
  // };
  // fetch('http://localhost:3000/data')
  //   .then(item => item.json())
  //   .then(res => render(res));

  // post data
  // const formContact = document.querySelector('.contact'),
  //   label = document.querySelector('.contact__label');

  // formContact.addEventListener('submit', e => {
  //   e.preventDefault();
  //   const formData = {
  //     name: formContact.name.value,
  //     phone: formContact.phone.value,
  //     agree: formContact.agree.checked
  //   };
  //   if (formContact.agree.checked) {
  //     fetch('http://localhost:3000/orders', {
  //       method: 'POST',
  //       body: JSON.stringify(formData),
  //       headers: {
  //         'Content-type': 'application/json'
  //       }
  //     })
  //       .then(res => res.json())
  //       .then(data => console.log(data));
  //   } else {
  //     label.style.color = 'red';
  //   }
  // });

  // slider
  // Procedural
  // const sliders = document.querySelectorAll('.prices__block'),
  //   sliderPrev  = document.querySelector('.prices__button-prev'),
  //   sliderNext  = document.querySelector('.prices__button-next');
  // let sliderIndex = 1;

  // const updateSlider = n => {
  //   if (n > sliders.length) {
  //     sliderIndex = 1;
  //   } else if (n < 1) {
  //     sliderIndex = sliders.length;
  //   }

  //   sliders.forEach(slider => slider.style.display = 'none');
  //   sliders[sliderIndex - 1].style.display = 'block';
  // };
  // updateSlider(sliderIndex);
  // const setIndex = n => {
  //   sliderIndex += n;
  //   updateSlider(sliderIndex);
  // };
  // sliderPrev.addEventListener('click', () => setIndex(-1));
  // sliderNext.addEventListener('click', () => setIndex(1));

  //OOP
  const sliders = document.querySelectorAll('.prices__block'),
    sliderPrev  = document.querySelector('.prices__button-prev'),
    sliderNext  = document.querySelector('.prices__button-next');
  const Slider = class {
    constructor(sliders, sliderPrev, sliderNext, sliderIndex = 1) {
      this.sliders = sliders;
      this.sliderPrev = sliderPrev;
      this.sliderNext = sliderNext;
      this.sliderIndex = sliderIndex;
    }
    updateSlider(n) {
      if (n > this.sliders.length) {
        this.sliderIndex = 1;
      } else if (n < 1) {
        this.sliderIndex = this.sliders.length;
      }
      this.sliders.forEach(slider => slider.style.display = 'none');
      this.sliders[this.sliderIndex - 1].style.display = 'block';
    }
    setIndex(n) {
      this.sliderIndex += n;
      this.updateSlider(this.sliderIndex);
    }
    emit() {
      this.sliderPrev.addEventListener('click', () => this.setIndex(-1));
      this.sliderNext.addEventListener('click', () => this.setIndex(1));
    }
  };

  const newSlider = new Slider(sliders, sliderPrev, sliderNext);
  newSlider.updateSlider(1);
  newSlider.emit();

  // func obj
  // const sliders = document.querySelectorAll('.prices__block'),
  //   sliderPrev  = document.querySelector('.prices__button-prev'),
  //   sliderNext  = document.querySelector('.prices__button-next');
  // const Slider = (sliders, sliderPrev, sliderNext, sliderIndex = 1) => ({
  //   sliders, sliderPrev, sliderNext, sliderIndex,
  //   updateSlider(n) {
  //     if (n > this.sliders.length) {
  //       this.sliderIndex = 1;
  //     } else if (n < 1) {
  //       this.sliderIndex = this.sliders.length;
  //     }
  //     this.sliders.forEach(slider => slider.style.display = 'none');
  //     this.sliders[this.sliderIndex - 1].style.display = 'block';
  //   },
  //   setIndex(n) {
  //     this.sliderIndex += n;
  //     this.updateSlider(this.sliderIndex);
  //   },
  //   emit() {
  //     this.sliderPrev.addEventListener('click', () => this.setIndex(-1));
  //     this.sliderNext.addEventListener('click', () => this.setIndex(1));
  //   }
  // });

  // const newSlider = Slider(sliders, sliderPrev, sliderNext);
  // newSlider.updateSlider(1);
  // newSlider.emit();
  // localStorage.setItem('Ivan', 131314132);
  // localStorage.setItem('Kate', 4132);
  // localStorage.setItem('Bill', 131314);
  // console.log(localStorage.getItem('Bill'));
});
